# 小花花“赚大钱”兼职平台-微信小程序
## 介绍

> 本文档为小花花“赚大钱”兼职平台服务端的安装部署教程，欢迎.

> 项目不足请多指教，任何问题请移步对应的仓库提交Issues

### 项目链接
本项目 一共分为小程序端和后台管理系统

#### [1.小程序端地址：<a href="https://gitee.com/wdwlch99/wx-program.git">https://gitee.com/wdwlch99/wx-program.git</a>

#### [2.后台地址：<br>

## 交流方式

### ！搭建遇到任何问题，请联qq:1448986387！

## 图片展示
### 前端
![小程序首页](%E6%95%88%E6%9E%9C%E5%9B%BE%E7%89%87/%E5%B0%8F%E7%A8%8B%E5%BA%8F-%E9%A6%96%E9%A1%B5.png)
![小程序详情页1](%E6%95%88%E6%9E%9C%E5%9B%BE%E7%89%87/%E5%B0%8F%E7%A8%8B%E5%BA%8F-%E8%AF%A6%E6%83%85%E9%A1%B51.png)
![小程序详情页2](%E6%95%88%E6%9E%9C%E5%9B%BE%E7%89%87/%E5%B0%8F%E7%A8%8B%E5%BA%8F-%E8%AF%A6%E6%83%85%E9%A1%B52.png)
![小程序详情页3](%E6%95%88%E6%9E%9C%E5%9B%BE%E7%89%87/%E5%B0%8F%E7%A8%8B%E5%BA%8F-%E8%AF%A6%E6%83%85%E9%A1%B53.png)
![小程序兼职广场](%E6%95%88%E6%9E%9C%E5%9B%BE%E7%89%87/%E5%85%BC%E8%81%8C%E5%B9%BF%E5%9C%BA.png)
![小程序我的兼职](%E6%95%88%E6%9E%9C%E5%9B%BE%E7%89%87/%E6%88%91%E7%9A%84%E5%85%BC%E8%81%8C.png)
![小程序我的发布](%E6%95%88%E6%9E%9C%E5%9B%BE%E7%89%87/%E6%88%91%E7%9A%84%E5%8F%91%E5%B8%83.png)
![小程序我的页面](%E6%95%88%E6%9E%9C%E5%9B%BE%E7%89%87/%E6%88%91%E7%9A%84%E9%A1%B5%E9%9D%A2.png)
![小程序我的信息](%E6%95%88%E6%9E%9C%E5%9B%BE%E7%89%87/%E6%88%91%E7%9A%84%E4%BF%A1%E6%81%AF.png)