let com = require('./utils/util.js')
App({
  com: com,
  onLaunch() {
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    
    // 登录
    wx.login({
      success: res => {
        console.log(res.code);
        wx.request({
          url: 'http://localhost:8089/wxpai/login/wxLogin?code='+res.code, //请求的接口地址
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          success:res => {
            console.log(res.data);
            if (res.data.code == 200) {
              if (res.data.data.openid) {
                //存储openid到全局变量，当然也可以同时存储到作用域中
                this.globalData.openid = res.data.data.openid
                this.globalData.session_key = res.data.data.session_key
                console.log(this.globalData.openid)
                console.log(this.globalData.session_key)
                wx.setStorageSync('openid', res.data.data.openid)
              }
            }
          }
        })
      }
    })
  },
  // login(cb){
  //   wx.login({
  //     success(res) {
  //       com.post('wx/user/login', { js_code: res.code }, function (res) {
  //         if (res.code == 1) {
  //           wx.setStorageSync("user", res.data)
  //           wx.setStorageSync("token", res.token)
  //           cb(res)
  //         }
  //       })
  //     }
  //   })
  // },
  //获取默认地址
  getMoren(id){
    if(id){
      com.post('user/address/get/id', { id: id }, function (res) {
        if (res.code == 1) {
          wx.setStorageSync("address", res.data)
        }
      })
    }
  },
  getRes(id){
    com.post('wx/user/get/info/wxid', { wx_id: id }, function (res) {
      if (res.code == 1) {
        wx.setStorageSync("res", res.data)
      }
    })
  },
  globalData: {
    userInfo: null,
    url: 'http://localhost:8089', //请求的url
    openid: '',
    session_key: '',
    token: ''
  }
})