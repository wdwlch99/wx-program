const app = getApp()
let _this;
Page({

  /**
   * 页面的初始数据
   */
  userInfo: {},
  hasUserInfo: false,
  canIUseGetUserProfile: false,
  data: {
    phone:'',
    dphone:'',
  },

  getPhoneNumber (e) {
    console.log(e.detail.code)
  },
  ddinput(e){
    let name = e.currentTarget.dataset.name;
    this.data[name] = e.detail.value;
    
    this.setData({
      phone: this.data.phone,
      dphone: this.data.dphone
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
    // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
        wx.setStorageSync("user", res.userInfo)
        wx.navigateBack({
          detal:1
        })
    //保存用户数据
    wx.request({
      url: app.globalData.url+'/api/user2/save',
      method: "POST", //提交方式
      data:{
        openid : app.globalData.openid,
        avatarurl :  res.userInfo.avatarUrl,
        nickname : res.userInfo.nickName
      },
      success:function(res){
 console.log(res)
      }
    });
        console.log(res.userInfo)
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
 
 
})
