const app = getApp()
let _this;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    page:1,
    load:false,
    size:10,
    tag:['兼职广场','我的发布','我的兼职'],
    sysList:[],
    wxList:[],
    flag:0,
    url:'get',
    wheres:"",
    sorts:"",
    fields:'',
    wx_id:wx.getStorageSync("user").id
  },
  sysListAll(){
    var that = this;
    wx.request({
      url: app.globalData.url+'/api/task/list2', 
      success:res => {
        console.log(res);
        that.setData({
          sysList : res.data
        })
      }
    })
  },
  navToArea(e){
    console.log(e.currentTarget.dataset.taskorder);
      wx.navigateTo({
        url: '/pages/order/detail/detail?data='+JSON.stringify(e.currentTarget.dataset.taskorder),
       
      })
    },
  changeTag(e) {
    let index = e.currentTarget.dataset.index
    this.setData({
      flag: e.currentTarget.dataset.index
    })
    if(index == 0){
      this.data.url = 'get'
      this.data.wheres = 'a_id=' + wx.getStorageSync("area").pk_id
      this.data.sorts = ""
      this.data.fields = ''
      // this.getList(0)
    }else if(index == 1){
      this.data.url = 'get2'
      this.data.fields = 'helplist.*,wxuser.phone,wxuser.dphone,wxuser.avatar_url,wxuser.nick_name'
      this.data.wheres = 'helplist.is_delete=0 and wx_id=' + wx.getStorageSync("user").id
      this.data.sorts = "helplist.create_time desc"
      // _this.getList(0)
    }else{
      this.data.url = 'get3'
      this.data.fields = 'helplist.*,wxuser.phone,wxuser.dphone,wxuser.avatar_url,wxuser.nick_name'
      this.data.wheres = 'helplist.is_delete=0 and jd_id=' + wx.getStorageSync("user").id
      this.data.sorts = "helplist.create_time desc"
      // this.getList(0)
      // this.getWxsmData()
    }
  },
  wx(){
    // console.log(this.data.taskOrder);
    wx.request({
      url: app.globalData.url+'/api/task/list2',
      success:res=>{
        console.log(res.data);
        this.setData({
          wxList : res.data,
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.sysListAll();
    this.wx();
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title:'小花花赚大钱兼职平台',
      path:'/pages/index/index'
    }
  }
})