const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  swtobz(e){
    wx.setStorageSync("bzflag", e.currentTarget.dataset.index)
    wx.switchTab({
      url: '/pages/banzu/banzu',
    })
  },
  // Phone(){
  //   // console.log(this.data.userList.phone);
  //   wx.makePhoneCall({
  //     phoneNumber: 085, //仅为示例，并非真实的电话号码
  //     success:res=>{
  //       console.log("去打电话");
  //     },
  //     fail:res=>{
  //       console.log("取消打电话");
  //     },
  //   })
  // },
  navTo(e) {
    app.com.navTo(e)
  },
  makePhone(){
    wx.makePhoneCall({
      phoneNumber: '085-1001011',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (wx.getStorageSync("user").nickName) {
      this.setData({
        userInfo: wx.getStorageSync("user")
      })
    } else {
      wx.navigateTo({
        url: '/pages/login/login',
      })
    }
    
  },

  /**
   * 生命周期函数--监听页面隐 藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})