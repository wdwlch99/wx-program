const app = getApp()
let _this;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // user:{
      nickname:"",
      phone:"",
      photo:"",
      name:"",
      sex:"",
      ec:"",
      ecPhone:"",
      address:"",
      openid:wx.getStorageSync("openid"),
      userList:[],
    // },
    temp:false
  },  
  getUserList(){
    var that = this;
    console.log(666);
    wx.request({
      url: app.globalData.url+'/api/user2/list2', 
      success:res => {
        console.log(res.data);
        console.log(this.name);
        that.setData({
          userList : res.data
        })
      }
    })
  },
  formSubmit(e){
    const user = e.detail.value;
    console.log(user);
    console.log(e.detail.value.nickname);
    wx.request({
      url: app.globalData.url+'/api/user2/update',
      data:{
        nickname:e.detail.value.nickname,
        phone:e.detail.value.phone,
        openid:wx.getStorageSync("openid"),
        ecPhone:e.detail.value.ecPhone,
          photo:e.detail.value.photo,
          name:e.detail.value.name,
          sex:e.detail.value.sex,
          ec:e.detail.value.ec,
          address:e.detail.value.address,
      },
      method:"POST",
      success:res=>{
        if(res.data.code==200){
          wx.showModal({
            title: '保存成功',
            content: 'success'
      })
      // this.getUserList();
      this.setData({
        // user:{
          nickname:"",
          phone:"",
          photo:"",
          name:"",
          sex:"",
          ec:"",
          ecPhone:"",
          address:"",
        // }
      })
    }
  }
  })
  },
  changeAvatar(){
    wx.chooseImage({
      count:1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: function(res) {
        wx.compressImage({
          src: res.tempFilePaths[0], // 图片路径
          quality: 50, 
          success(res){
            _this.setData({
              photo: res.tempFilePath,
              temp: true
            })
          },
          fail(res){
            console.log(res)
          }
        })
        // _this.setData({
        //   'user.photo': res.tempFilePaths[0],
        //   temp: true
        // })
        
      },
    })
  },
  upload(cb){
    if(this.data.temp){
      wx.uploadFile({
        url: app.com.API + 'file/upload', 
        filePath: this.data.user.photo,
        name: 'file',
        formData: {
          wx_id: wx.getStorageSync("user").id,
          a_id: wx.getStorageSync("area").pk_id,
          is_temp:0
        },
        success(res) {
          console.log();
          let red = JSON.parse(res.data)
          if (red.code == 1) {

            let uinfo = wx.getStorageSync("user");
            uinfo.photo = red.data.url;
            wx.setStorageSync("user", uinfo)
            _this.setData({
              'user.photo': red.data.url
            })
            cb(true)
          }else{
            cb(false)
          }
        }
      })
    }else{
      cb(false)
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options){
    _this= this;
    console.log(78);
    this.getUserList();
    console.log(90);
    this.setData({
      user:wx.getStorageSync("user")
    })
  },
})