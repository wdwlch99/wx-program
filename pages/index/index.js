//index.js
//获取应用实例
const app = getApp()
let _this;
Page({
  data: {
    imgurls:[],
    isFirst:true,
    sysList:[],
    list:[
    ],
    server:[],
    noticeList:[]
  },
  notice(){
    var that = this;
    wx.request({
      url: app.globalData.url+'/api/notice/list', 
      success:res => {
        console.log(res.data);
        that.setData({
          noticeList : res.data
        })
      }
    })
  },
  navToArea(e){
  console.log(e.currentTarget.dataset.taskorder);
    wx.navigateTo({
      url: '/pages/order/detail/detail?data='+JSON.stringify(e.currentTarget.dataset.taskorder),
     
    })
  },
  sysListAll(){
    var that = this;
    wx.request({
      url: app.globalData.url+'/api/task/list2', 
      success:res => {
        console.log(res);
        that.setData({
          sysList : res.data
        })
      }
    })
  },
  publist(){
    wx.navigateTo({
      url: '/pages/pub/pub',
    })
  },
  onLoad: function (options) {
    _this = this
    _this.notice()
  },
  getCarousel(){
    app.com.post('calousels/get',{a_id:wx.getStorageSync("area").pk_id},function(res){
      if(res.code == 1){
        _this.setData({
          imgurls:res.data.list
        })
      }
    })
  },

  onPullDownRefresh(){
    this.getServer(wx.getStorageSync('dl').pk_id)
  },
  onShow(){
    this.sysListAll()
    if(!this.data.isFirst){
      this.checkArea()
    }
  },
  onShareAppMessage(){
    return {
      title:'大花花“赚小钱”',
      path:'/pages/index/index'
    }
  }
  
})
