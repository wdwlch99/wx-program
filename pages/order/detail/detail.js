//index.js
//获取应用实例
const app = getApp()
let _this;
Page({
  data: {
    imgurls:[],
    isFirst:true,
    list:[
    ],
    userTaskList:[],
    taskOrder:"",
    server:[],
    userList:"",
    state:""
  },
  updateUserState2(){
    wx.request({
      url: app.globalData.url+'/api/task/update',
      data:{
        taskOrder:this.data.taskOrder,
        state:0,
      },
      method:"POST",
      success: res => {
        wx.redirectTo({
          url: '../../banzu/banzu',
          success: res => {
            wx.showToast({
              title: '已取消',
              icon: 'none',
              duration: 2000
            })
          }
        })
      }
      })
  },
  cancel2(){
        wx.redirectTo({
          url: '../../banzu/banzu',
        })
  },
  getUserState(){
    var that = this;
    console.log(this.data.taskOrder);
    wx.request({
      url: app.globalData.url+'/api/task/update', 
      success:res => {
        console.log(res.data.data.state);
        // console.log(this.name);
        that.setData({
          taskOrder:this.data.taskOrder,
          state : res.data.data.state,
        })
      }
    })
  },
  wx(){
    // console.log(this.data.taskOrder);
    wx.request({
      url: app.globalData.url+'/api/task/getInfo?taskOrder='+this.data.taskOrder,
      success:res=>{
        console.log(res.data.data.state);
        this.setData({
          state : res.data.data.state,
        })
      }
    })
  },
  // 修改用户任务状态
  updateUserState(e){
    console.log(e);
    wx.request({
      url: app.globalData.url+'/api/task/update',
      data:{
        state:1,
        taskOrder:this.data.taskOrder
      },
      method:"POST",
      success: res => {
        wx.redirectTo({
          url: '../../banzu/banzu',
          success: res => {
            wx.showToast({
              title: '报名成功',
              icon: 'none',
              duration: 2000
            })
          }
        })
      }
        })
      },
  email(){
    let email=this.data.userList.email;
    wx.showModal({
      title: '投简历',
      content: '简历发送到邮箱：'+email,
      success (res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  Phone(){
    console.log(this.data.userList.phone);
    wx.makePhoneCall({
      phoneNumber: this.data.userList.phone, //仅为示例，并非真实的电话号码
      success:res=>{
        console.log("去打电话");
      },
      fail:res=>{
        console.log("取消打电话");
      },
    })
  },
  getUserId(e){
    console.log(e.userId)
    wx.request({
      url: app.globalData.url+'/api/user/getInfo2?userId='+e.userId,
      success:res => {
        console.log(e)
        console.log(res.data.data);
        console.log(this.userTaskList);
        this.setData({
          userList : res.data.data
        })
      }
    })
  },
  getUserTask(){
    console.log(this.data.taskOrder);
    wx.request({
      url: app.globalData.url+'/api/user/task/getInfo?taskOrder='+this.data.taskOrder,
      success:res => {
        console.log(35);
        console.log(res.data);
        this.setData({
          userTaskList : res.data.data
        })
        this.getUserId(res.data.data);
      }
    })
  },
  sysGetById(e){   
    wx.request({
      url: app.globalData.url+'/api/task/getInfo?taskOrder='+this.data.taskOrder, 
      success:res => {
        console.log(res.data.data);
        _this.setData({
          sysList : res.data.data
        })
      }
    })
  },
  navToArea(){
    wx.navigateTo({
      url: '/pages/area/area',
    })
  },
  publist(){
    wx.navigateTo({
      url: '/pages/pub/pub',
    })
  },
  onLoad: function (options) {
    this.getUserTask();
    _this = this
    if(options.data){
      let taskOrder = JSON.parse(options.data)
      // console.log(taskOrder);
      _this.setData({
        taskOrder:taskOrder
      })
    }
    this.sysGetById();
    this.getUserTask();
    this.wx();
    // console.log(12333);
    // this.getUserState();
  },

  onPullDownRefresh(){
    this.getServer(wx.getStorageSync('dl').pk_id)
  },
  onShareAppMessage(){
    return {
      title:'大花花“赚小钱”',
      path:'/pages/index/index'
    }
  }
  
})
